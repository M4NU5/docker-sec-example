# docker-sec-example

This repository contains a Dockerfile which extends the official nginx container image to create a microservice. The microservice presents a website advertising some example services.

To build the container locally:

`docker build --tag local/sec-example .`

To run the container in the foreground:

`docker run -p 80:80 -p 443:443 --interactive --tty local/sec-example`

Once the container is running you should be able to navigate to http://localhost or https://localhost to view it.

Note about ports:

* If you're attempting to use privileged ports you may find that you get an error when starting the container, if this is the case I suggest you use the following instead:
  * `docker run -p 8080:80 -p 8443:443 --interactive --tty local/sec-example`
  * Navigate to http://localhost:8080 or https://localhost:8443

The purpose of this exercise is to ensure a secure workload is being deployed for this microservice. You should undertake the following tasks:

* Extend the GitLab CI pipeline to run appropriate scans to ensure sufficient security
  * It's up to you what tooling you use; please research, select appropriately and document your reasons
* Write down at least 2 general advisories for improving the security of the container (i.e. not the output of the security tooling)

**Important notes:**

* Please don't spend longer than 2 hours completing this task.
* Don't hide your test commits or pipelines - it's very useful to see the evolution of your creation.
* You don't need to fix the issues that are uncovered.

*Hint: Check out what tooling is provided by GitLab first*
