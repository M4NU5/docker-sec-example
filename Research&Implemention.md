# Research & Implemention of CI/CD 
Tags: #CI/CD
Related to:
Previous:

---

### Research Steps
**Reading GitLab Documentation**
[GitLab Getting Started](https://docs.gitlab.com/ee/ci/quick_start/index.html)
[GitLab .yml keywords](https://docs.gitlab.com/ee/ci/yaml/index.html)
Keyword | Description
-|-
rules | Spesify when to run and skip jobs
Cache / Artifacts | Keep information across jobs
[GitLab Secure Your Applicaiton](https://docs.gitlab.com/ee/user/application_security/)

**GitLab Youtube**
[GitLab adding security to CI/CD](youtube.com/watch?v=Fd5DhebtScg)

**Read up on Top 10 Security Risks for CI/CD**
[OWASP Top 10 CI/CD](https://owasp.org/www-project-top-10-ci-cd-security-risks/)
[Github OWASP TOP 10 CI/CD](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks)
Short extract for OWASP 
OWASP | Title | Definition
 -|-|-
1|[Insufficient Flow Control](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-01-Insufficient-Flow-Control-Mechanisms.md) | Ability for an attacker to single handadly push malicious code to production
2|[Inadequate IAM](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-02-Inadequate-Identity-And-Access-Management.md) | The existance or poorly managed identities (Both People & Programmic) increases the potential of compromise and damage
3 - |[Dependency Chain Abuse](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-03-Dependency-Chain-Abuse.md) | Attackers compromising packages that are fetched and then executed locally
4|[Poisoned Pipeline Execution](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-04-Poisoned-Pipeline-Execution.md) | Attackers compromising source control systems that then manipulates the build process
5|[Insufficient Pipeline-Based Access Controls](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-05-Insufficient-PBAC.md)|Pipeline execution nodes have access to numerous resources. e.g. Highly confiedntial pipeline running alongside a public pipeline
6 - |[Insufficient Credential Hygiene](https://github.com/OWASP/www-project-top-10-ci-cd-security-risks/blob/main/CICD-SEC-06-Insufficient-Credential-Hygiene.md)| Insecure Secrets Management & overly permissive credentials

[Security Tests](https://docs.gitlab.com/ee/user/application_security/configuration/) Natively supported by GitLab

**Comments on each step**
*OWASP 1, 2 & 5* - NO
Approval Controls, Make protected branch -> Main is protected 
[Merge SoD](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
As a solo actor I will leave out 1 & 2. As these address segmentation of duties.
Theoretically I could create a secondry gitlab account add that account as the approval acocunt. 
I want to note: Segmentation of duties is a key component in the CI/CD pipeline.

*OWASP 4* - NO
An interesting attack vector, potentially mitigated through SoD.
Perhaps externalise gitlab-ci.yml to another branch? And merge in with trigger?

OWASP 3 - Yes
[Dependancy Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html)
Add Dependancy check in build process

OWASP 6 - Yes
[Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html)
Add secret check in build process
```
include:
- template: Security/Secret-Detection.gitlab-ci.yml

- secret_detection:
	stage: test
```
**2 Build Commits**

Dependancy Scanning doesnt seem to be running???
After further research it is suggested i use Container scanning instead for docker images
```
include:
- template: Security/Container-Scanning.gitlab-ci.yml

container_scanning:
	stage: test
	variables:
		CS_IMAGE: $CONTAINER_TEST_IMAGE
```

[**Static code analysis**](https://docs.gitlab.com/ee/user/application_security/sast/index.html)
Catches common vaulnerabilities like 
- Buffer overflows
- SQL injection flaws
- Does however have a prepencity for a high number of false positives with a relatively small number of valn types detected
https://docs.gitlab.com/ee/user/application_security/sast/index.html




---
# Referances

### Documentation
[Docker](https://docs.docker.com/engine/reference/commandline/docker/)
[GitLab](https://docs.gitlab.com/)
